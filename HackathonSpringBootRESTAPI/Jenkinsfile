def projectName = 'dg3-trade-api'
def subDir = 'HackathonSpringBootRESTAPI'

pipeline {
  agent any

  stages {
    stage('Test') {
      steps {
        dir("${subDir}") {
          sh 'chmod a+x gradlew'
          sh './gradlew test'
	}
      }
    }

    stage('Build') {
      steps {
        dir("${subDir}") {
          sh './gradlew build'
        }
      }
    }

    stage('Build Container') {
      steps {
        dir("${subDir}") {
          sh "docker build -t ${projectName}:0.0.${currentBuild.number} ."
        }
      }
    }

    stage('Deploy Container') {
      steps {
        sh "docker stop ${projectName}|| echo 'No container to stop'"
        sh "docker rm  ${projectName} || echo 'No container to remove'"
        sh "docker run --name ${projectName} --restart unless-stopped -d -p 8080:8080 --net mongo-net -e DB_HOST=mongo ${projectName}:0.0.${currentBuild.number}"
      }
    }
  }

  post {
    always {
      archiveArtifacts artifacts: "${subDir}/build/libs/**/*.jar", fingerprint: true
      archiveArtifacts artifacts: "${subDir}/build/jacoco/**/*"
      archiveArtifacts "${subDir}/build/reports/**/*"
    }
  }
}
