package com.citi.hackathon.HackathonSpringBootRESTAPI.TradeRepository;

import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepo extends MongoRepository<Trade, ObjectId> {

}