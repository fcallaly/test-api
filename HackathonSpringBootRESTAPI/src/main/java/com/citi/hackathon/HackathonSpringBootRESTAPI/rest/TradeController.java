package com.citi.hackathon.HackathonSpringBootRESTAPI.rest;

import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.HackathonSpringBootRESTAPI.entities.Trade;
import com.citi.hackathon.HackathonSpringBootRESTAPI.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@RequestMapping("/trade")
public class TradeController {

    @Autowired
    private TradeService tradeService;

    @RequestMapping(value = "/getAlltrade", method = RequestMethod.GET)
    public Collection<Trade> getTrade() {
        return tradeService.getTrade();
    }

    @RequestMapping(value = "/getTradeByID/{id}", method = RequestMethod.GET)
    public Optional<Trade> getTradeById(@PathVariable("id") ObjectId id) {
        return tradeService.getTradeById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade t) {
        tradeService.addTrade(t);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public String modifyTradeById(@PathVariable("id") ObjectId id, @RequestBody Trade t) {
        t.setId(id);
        tradeService.modifyTradeById(t);
        return id + " Updated successfully";
    }

    @DeleteMapping("/delete/{id}")
    public String deleteTrade(@PathVariable("id") ObjectId id) {
        tradeService.deleteTrade(id);
        return id + " Has been Deleted";
    }

    /*
     * @PutMapping("/update/{id}") public Trade update(@RequestBody Object id) {
     * return tradeService.saveOrUpdate(id);
     * 
     * }
     */

}