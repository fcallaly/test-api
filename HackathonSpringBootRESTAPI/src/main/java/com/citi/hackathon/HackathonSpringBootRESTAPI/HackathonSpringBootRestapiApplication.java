package com.citi.hackathon.HackathonSpringBootRESTAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HackathonSpringBootRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HackathonSpringBootRestapiApplication.class, args);
	}

}
